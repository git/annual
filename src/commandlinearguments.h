#ifndef COMMANDLINEARGUMENTS_H
#define COMMANDLINEARGUMENTS_H

//class QSettings;
#include <QSettings>
#include <QString>

class CommandLineArguments 
{
	bool light;
	bool quiet;
	QString config;

	public:
	CommandLineArguments();
	bool isQuiet() const { return quiet; }
	bool isLight() const { return light; }
	//bool hasConfigFile() const { return !config.isEmpty(); }
	//const QString& getConfigFile() const { return config; }
	QSettings* createSettings(QObject* parent) const;
};


#endif /* COMMANDLINEARGUMENTS_H */
