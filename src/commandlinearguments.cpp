#include "commandlinearguments.h"
#include <QCommandLineParser> 
#include <QFile>
#include "ioexception.h"

CommandLineArguments::CommandLineArguments() 
{
	QCommandLineParser parser;
	parser.setApplicationDescription(QObject::tr("Keeps track of all your anniversaries and hopefully reminds you at the right time."));
	parser.addHelpOption();
	parser.addVersionOption();

	QCommandLineOption lightOption(QStringList() << "l" << "light", QObject::tr("Do not use graphical interface - just output to console"));
	QCommandLineOption quietOption(QStringList() << "q" << "quiet", QObject::tr("Only popup a window if there is something to remind me of"));
	QCommandLineOption configOption(QStringList() << "c" << "config", QObject::tr("Instead of the default settings, use another <settings-file>"), QObject::tr("settings-file"));

	parser.addOption(lightOption);
	parser.addOption(quietOption);
	parser.addOption(configOption);
	parser.process(*QCoreApplication::instance());

	light = parser.isSet(lightOption);
	quiet = parser.isSet(quietOption);
	config = parser.value(configOption);

	if(!config.isEmpty() && !QFile::exists(config)) 
		throw IOException(QObject::tr("Settings File %1 does not exists").arg(config));

}
QSettings* CommandLineArguments::createSettings(QObject* parent) const
{
	return config.isEmpty() 
		? new QSettings(parent)
		: new QSettings(config, QSettings::IniFormat, parent);
}
