/* annual - Reminder for annual events
 * Keeps track of all your anniversaries and hopefully reminds you at the right time.
 * Copyright (C) 2011 Dominik Köppl
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QCoreApplication>
#include <QTextStream>

#include "mainwindow.h"
#include "config.h"


#include <QTranslator>
#include <QLibraryInfo>
#include <memory>

#include "commandlinearguments.h"

int main_cli(const CommandLineArguments& args);

static void showHelp()
{
	QTextStream cout(stdout, QIODevice::WriteOnly);
	cout << APPLICATION_NAME << " - " << APPLICATION_VERSION << endl
		<< QObject::tr("Arguments: ") << endl
		<< "  -h\t\t" << QObject::tr("Print Help (this message) and exit") << endl 
		<< "  -v\t\t" << QObject::tr("Print version information and exit") << endl 
		<< "  -l\t\t" << QObject::tr("Do not use graphical interface - just output to console") << endl
		<< "  -q\t\t" << QObject::tr("Only popup a window if there is something to remind me of") << endl;
}

void init_application() {
	QCoreApplication& app = *QCoreApplication::instance();
	QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
	QCoreApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
	QCoreApplication::setApplicationName(APPLICATION_NAME);

	QTranslator qttranslator;
	qttranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	QCoreApplication::instance()->installTranslator(&qttranslator);


	QTranslator translator;
	translator.load(":/translations/" APPLICATION_UNIXNAME "_" + QLocale::system().name());
	QCoreApplication::instance()->installTranslator(&translator);

}

int main(int argc, char *argv[])	// TODO: -v -> version!
{
	std::auto_ptr<QCoreApplication> app(new QCoreApplication(argc, argv));
	init_application();

	CommandLineArguments args;

	if(args.isLight())
		return main_cli(args);
	app.reset();
	QApplication appgui(argc,argv);
	appgui.setWindowIcon(QIcon(":/icons/program.png"));
	init_application();
	try
	{
		MainWindow w(args);
		w.show();
		return qApp->exec();
	}
	catch(int)
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
